# Class Organization
If I look at a class I want to see what it does and the method I have to interface with. So I
prefer having methods above variables. You don't need to know the member variables when interfacing
with a class.

This leads to the second take: public code should be above private code. Same argument here.

## Encapsulation
Just never use protect! This will always lead into bad design. For helper methods just create a
separate namespace or class.