# Conclusion
1. when possible use async for IO-bound tasks
2. only use a thread for a long running algorithm

For synchronization use data owning synchronization mechanisms like channels or a data owning
mutex. This will lead to easy-to-use and hard-to-missuse synchronization mechanisms.

Look at Rust mutex for example. You cannot use the containing object without locking the mutex.
No more wondering where to lock the mutex, if the mutex has to be locked or is already locked!