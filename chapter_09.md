# Clean Tests
Creating a custom DSL for tests can be time consuming. Just stick to easy and fast written tests.

To make the test easy to understand incorporate the terms `given`, `when` and `then` somehow.

For c++ with `GTest` I state the `given` in the test fixture. The `when` part is the test method
name (when calling X with Y => xWithY). The `then` is simply the test methods body.
```c++
class MyClass {
public:
    explicit MyClass(std::vector<int> vec) : m_vec{vec} {}
    void doSomething(int);
private:
    std::vector<int> m_vec;
};

struct MyClass_vectorWithOneElement : testing::Test {
    MyClass object{std::vector{42}};
};

TEST_F(MyClass_vectorWithOneElement, doSomethingWith13) {
    object.doSomething(13);
}
```

For Rust with [rstest](https://docs.rs/rstest/latest/rstest/) I state the `given` as a `mod` containing the
[fixture](https://docs.rs/rstest/latest/rstest/attr.fixture.html). For `when` and `then` I prefer the same a c++.
```Rust
struct MyClass {
    vec: Vec<i32>
}

impl MyClass {
    pub fn do_something(&mut self, i32) {}
}

#[cfg(test)]
mod tests {
    mod vector_with_one_element {
        use rstest::*;

        use super::super::*;

        #[fixture]
        fn my_class() -> MyClass {
            MyClass {
                vec: vec![42]
            }
        }

        #[rstest]
        fn do_something_with_13(mut my_class: MyClass) {
            my_class.do_something(13);
        }
    }
}
```

If I create data-driver tests in Rust with [rstest](https://docs.rs/rstest/latest/rstest/) I add some `given` and the `with` as
a `#[case]`.
```Rust
struct MyClass {
    vec: Vec<i32>
}

impl MyClass {
    pub fn do_something(&mut self, i32) {}
}

#[cfg(test)]
mod tests {
    use rstest::*;

    use super::*;

    #[fixture]
    fn my_vec() -> Vec<i32> {
        Default::default()
    }

    #[fixture]
    fn my_class_with_fixture_injection(my_vec: Vec<i32>) -> MyClass {
        MyClass {
            vec: my_vec
        }
    }

    #[rstest]
    #[case(vec![], 7)]
    #[case(vec![1], 42)]
    #[case(vec![1, 2, 3], 1337)]
    fn do_something(#[case] given_vec: Vec<i32>, #[with(given_vec)] mut my_class_with_fixture_injection: MyClass, #[case] with_value: i32) {
        my_class_with_fixture_injection.vec = given_vec;
        my_class_with_fixture_injection.do_something(with_value);
    }

    /* alternative without fixture injection  */
    
    #[fixture]
    fn my_class_without_fixture_injection() -> MyClass {
        MyClass {
            vec: Default::default()
        }
    }

    #[rstest]
    #[case(vec![], 7)]
    #[case(vec![1], 42)]
    #[case(vec![1, 2, 3], 1337)]
    fn do_something(mut my_class_without_fixture_injection: MyClass, #[case] given_vec: Vec<i32>, #[case] with_value: i32) {
        my_class.vec = given_vec;
        my_class.do_something(with_value);
    }
}
```