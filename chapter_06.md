# Conclusion
I would avdvise to always use a data driven design aproach. It helps gaining performance for free
because it is cache friendly. Also it helps separating working code (classes) from data (records)
properly. In most cases you don't need to add multiple hundeds of new classes for new
functionality anyway.