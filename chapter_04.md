# Good Comments
First my little take on comments: Just try to replace comments with constants and anonymous
methods. Make the code self descriptive!

Personally I need to write a comment like every 50k lines of code because neither a constant nor
an anonymous method could be used.

_Note:_ Documentation comments (like doxygen or rustdoc) are always fine.

## Explanation of Intent
Somehow the book here just uses comments instead of code to state logic. In the first example the
comment on the `return 1;` could simply be replaced by a constant value:
```Rust
const WE_ARE_GREATER_BECAUSE_WE_ARE_THE_RIGHT_TYPE: i32 = 1;
return WE_ARE_GREATER_BECAUSE_WE_ARE_THE_RIGHT_TYPE;
```

In the second example the commend could be simply replaced by a private or an anonymous method
```c++
const auto createLargeNumberOfThreadsToGetRaceCondition = [&]() {
    ...
};
```

## Clarification
As obove just use constants and/or anonymous methods instead of comments!

## Warning of Consequences
In the second example the comment could also be replaced by wrapping the code in a lambda.
Might look weird at first but gets the job of not using comments done.

## TODO Comments
Instead of `todo`-comments use your ticket system (issues)!

For application breaking todos Rust has a very good system. With application breaking todos I
mean code that would just not work as required. In that case the code should not run at all!
Rust just panics the running thread instead of leaving the thread in a harming state. A
`"TODO finish implementation"`-comment would not have the same effect.

## Amplification
And a nother example which could be solved an anonymous method.

# Bad Comments
## Noise Comments
Here even the `// normal. someone stopped the request.` comment can be replaced by a unused
constant or anonymous method.

## Closing Brace Comments
For c++ it makes sense to add the namespace name to the closing bracked. Since names can be large
a notation which namespace is closed is quite helpful.

Other languages don't have this problem.