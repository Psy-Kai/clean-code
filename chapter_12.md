# No Duplication
The last example uses protected methods. Just don't do this. If needed create a new class with this
methods and inject it.

Deep class hierarchies are bad OOP practice. For GUI-widgets they are usefull. For nearly all other
purposes just use encapsulation over inheritence.

A well designed class hierarchy is 2 steps deep:
1. Interface
2. Implementation

# Minimal Classes and Methods
Here the book shoots agains data driven design. But functional and procedual code with data driven
design will lead to an overall better design.