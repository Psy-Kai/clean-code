# Avoid Encodings
## Member Prefixes
First the assumption is made that everyone prefixes the getter methods with `get`, e.g.
`getWidth()`. I don't like prefixing my getter with `get`. I just want to write `width()`. Without
 using a member prefix like `m_` this is not possible!

Additionally I would just advide not to use any getter/setter methods if possible! Most of the time
just calling a function and passing data is enough. There are limited cases where you actually need
getter/setter!

## Interfaces and Implementations
Naming the implementation of an interface `*Imp` or `C*` is as bad as prefixing the interface with
`I*`. Just use namespaces!

```c++
class IShapeFactory { ... };

class ShapeFactory : public IShapeFactory { ... };
```
could be neatfully rewritten to
```c++
namespace shapefactory {

class Interface { ... };

} // shaprefactory

class ShapeFactory : public shapefactory::Interface { ... };

```

# Method Names
Here the `javabean standard` is cited?! But I don't like prefixing getter with `get` or
beeing forces to prefix boolean getter with `is`. Only prefix a method returning a boolean with
`is` if it helps reading/unterstanding the function/code!

# Add Meaningful Context
Listing 2-2 is just really bad code design! You could easily rewrite the code to be 2 classes and
a single record of data.
```c++
namespace guessstaticsmessage {

struct Statistics {
    std::string number;
    std::string verb;
    std::string pluralModifier;
};

namespace helper {

class Interface {
public:
    virtual ~Interface() = default;
    virtual Statistics thereAreManyLetters(int count) const = 0;
    virtual Statistics thereIsOneLetter() const = 0;
    virtual Statistics thereAreNoLetters() const = 0;
};

} // helper

struct Helper : helper::Interface {    
    Statistics thereAreManyLetters(int count) const {
        return Statistics {
            std::to_string(count),
            "is",
            "s"
        };
    }
    Statistics thereIsOneLetter() const {
        return Statistics {
            "1",
            "is",
            ""
        };
    }
    Statistics thereAreNoLetters() const {
        return Statistics {
            "no",
            "are",
            "s"
        };
    }
};

} // guessstaticsmessage

class GuessStaticsMessage {
public:
    explicit GuessStaticsMessage(guessstaticsmessage::helper::Interface &helper) : m_helper{helper}
    {}

    std::string make(char candidate, int count) const {
        const auto statistics = createPluralDependentMessageParts(count);
        return std::format("There {} {} {}{}", statistics.verb, statistics.number, candidate
            statistics.pluralModifier        
        );
    }
private:
    guessstaticsmessage::Statistics createPluralDependentMessageParts(int count) const {
        switch (count) {
            case 0:
                return m_helper.thereAreNoLetters();
            case 1:
                return m_helper.thereIsOneLetter();
            default:
                return m_helper.thereAreManyLetters(count);
        }
    }

    guessstaticsmessage::helper::Interface &m_helper;
};
```

Using this design testing and extending gets a lot easier. It also seperates properly between
working code (classes) and data (records).

_Note:_ Helper is not a good name here, I know. I used the name to get the overall design across.