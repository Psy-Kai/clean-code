# Switch Statements
The book states that the function in Listing 3-4 is large... I don't think that 10 lines of code
in a function body is too much.

Especially when coding in Rust and have multiple encapsulated enums a big and declarative match
statement might not be avoidable. But since this match can be made declarative I don't think that
it would lead to bad code. Just call methods in every match arm and the code is just fine.

# Function Arguments
## Triads
Soo... Here I would just state the rule: never more than 3 arguments (except this/self). If you
need to pass more arguments maybe a record with data is a good solution.

## Verbs and Keywords
Just a better solution for `assertExpectedEqualsActual` might be
```c++
assert(Expected(value)).equals(actual)`
```
or
```c++
expect(value).equals(actual).assert()
```

# Have No Side Effects
"Having No Side Effects" is just not possible. It is a advice aimed to bad OOP practices! Your
system will allways have some side effects. Even logging into a file is a side effect.

Whats meant here is that you should prefer c++ `const` or Rust `non-mut` whenever possible. if you
don't follow most bad OOP practices your methods will automatically limit side effects to
`this`/`self`.

## Output Arguments
The example given is just plain bad OOP practice! Always split working code (classes) and data
(records)!

In the example given the `StringBuffer` is just data. Use a separate class to append the data.
Don't derive from `StringBuffer` to attach any methods! For that specific case just return
the modified data. The compiler/interpreter will optimize it for you.

```c++
struct StringBuffer {};

StringBuffer &appendFooter(StringBuffer&);
```
In c++ you can just input a reference to the object to modify and the return the same reference
back. Now you start writing stuff like
```c++
appendFooter(appendHeader(buffer));
```
The next trick would be to just encapsulte these `append*`-methods into a separate class:
```c++
class Appender {
public:
    explicit Appender(StringBuffer &buffer) : m_buffer{buffer} {}
    Appender &footer();
    Appender &header();
private:
    StringBuffer &m_buffer;
};

StringBuffer buffer;
Appender append{buffer};
append.footer().header();
```
Now we have a very good code design. No need to read the calling methods inside out like before.
Also testing gets very easy since we moved the functionality to modify the buffer into a separate
class.